/**
 * Created by Conrad on 5/10/2017.
 */
var express = require('express');
var router = express.Router();
var movie_dal = require('../model/movie_dal');

router.get('/all', function(req,res){
    movie_dal.getAll(function(err, result){
        if(err){
            res.send(err);
        }
        else{
            res.render('movie/movieViewAll', {'result': result});
        }
    });
});

router.get('/', function(req, res){
    if(req.query.movie_id == null){
        res.send('movie_id is null');
    }
    else {
        movie_dal.getById(req.query.movie_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else{
                res.render('movie/movieViewByID', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    movie_dal.getAll(function(err, result){
        if(err){
            res.send(err);
        }
        else {
            res.render('movie/movieAdd', {'movie': result});
        }
    });
});

router.get('/update', function(req, res){
    movie_dal.update(req.query, function(err, result){
        res.redirect(302, '/movie/all');
    });
});

router.get('/insert', function(req,res){
    if(req.query.name == null){
        res.send('Name must be provided');
    }
    else if(req.query.year == null){
        res.send('Year must be provided');
    }
    else {
        movie_dal.insert(req.query, function(err, result){
            if(err){
                res.send(err);
            }
            else {
                res.redirect(302, '/movie/all');
            }
        });
    }
});

router.get('/delete', function(req, res){
    if(req.query.movie_id == null){
        res.send('movie_id is empty');
    }
    else {
        movie_dal.delete(req.query.movie_id, function(err, result){
            if(err){
                res.send(err);
            }
            else{
                res.redirect(302, '/movie/all');
            }
        });
    }
});

router.get('/edit', function (req, res){
    if(req.query.movie_id == null) {
        res.send('An movie_id is required');
    }
    else {
        movie_dal.edit(req.query.movie_id, function(err, result){
            res.render('movie/movieUpdate', {'movie': result[0][0]});
        })
    }
});

module.exports = router;