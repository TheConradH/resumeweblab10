/**
 * Created by Conrad on 5/10/2017.
 */
var express = require('express');
var router = express.Router();
var movie_dal = require('../model/actor_dal');

router.get('/all', function(req,res){
    actor_dal.getAll(function(err, result){
        if(err){
            res.send(err);
        }
        else{
            res.render('actor/actorViewAll', {'result': result});
        }
    });
});

router.get('/', function(req, res){
    if(req.query.actor_id == null){
        res.send('actor_id is null');
    }
    else {
        actor_dal.getById(req.query.actor_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else{
                res.render('actor/actorViewByID', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    actor_dal.getAll(function(err, result){
        if(err){
            res.send(err);
        }
        else {
            res.render('actor/actorAdd', {'actor': result});
        }
    });
});

router.get('/update', function(req, res){
    actor_dal.update(req.query, function(err, result){
        res.redirect(302, '/actor/all');
    });
});

router.get('/insert', function(req,res){
    if(req.query.first_name == null){
        res.send('First name must be provided');
    }
    else if(req.query.last_name == null) {
        res.send('Last name must be provided');
    }
    else if(req.query.age == null){
        res.send('Age name must be provided');
    }
    else {
        actor_dal.insert(req.query, function(err, result){
            if(err){
                res.send(err);
            }
            else {
                res.redirect(302, '/actor/all');
            }
        });
    }
});

router.get('/delete', function(req, res){
    if(req.query.actor_id == null){
        res.send('actor_id is empty');
    }
    else {
        actor_dal.delete(req.query.actor_id, function(err, result){
            if(err){
                res.send(err);
            }
            else{
                res.redirect(302, '/actor/all');
            }
        });
    }
});

router.get('/edit', function (req, res){
    if(req.query.movie_id == null) {
        res.send('An movie_id is required');
    }
    else {
        movie_dal.edit(req.query.movie_id, function(err, result){
            res.render('movie/movieUpdate', {'movie': result[0][0]});
        })
    }
});

module.exports = router;