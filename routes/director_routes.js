/**
 * Created by Conrad on 5/10/2017.
 */
var express = require('express');
var router = express.Router();
var movie_dal = require('../model/director_dal');

router.get('/all', function(req,res){
    director_dal.getAll(function(err, result){
        if(err){
            res.send(err);
        }
        else{
            res.render('director/directorViewAll', {'result': result});
        }
    });
});

router.get('/', function(req, res){
    if(req.query.director_id == null){
        res.send('director_id is null');
    }
    else {
        director_dal.getById(req.query.director_id, function(err, result){
            if(err) {
                res.send(err);
            }
            else{
                res.render('director/directorViewByID', {'result': result});
            }
        });
    }
});

router.get('/add', function(req, res){
    director_dal.getAll(function(err, result){
        if(err){
            res.send(err);
        }
        else {
            res.render('director/directorAdd', {'director': result});
        }
    });
});

router.get('/update', function(req, res){
    director_dal.update(req.query, function(err, result){
        res.redirect(302, '/director/all');
    });
});

router.get('/insert', function(req,res){
    if(req.query.first_name == null){
        res.send('First name must be provided');
    }
    else if(req.query.last_name == null) {
        res.send('Last name must be provided');
    }
    else if(req.query.age == null){
            res.send('Age name must be provided');
    }
    else {
        director_dal.insert(req.query, function(err, result){
            if(err){
                res.send(err);
            }
            else {
                res.redirect(302, '/director/all');
            }
        });
    }
});

router.get('/delete', function(req, res){
    if(req.query.director_id == null){
        res.send('director_id is empty');
    }
    else {
        director_dal.delete(req.query.director_id, function(err, result){
            if(err){
                res.send(err);
            }
            else{
                res.redirect(302, '/director/all');
            }
        });
    }
});

router.get('/edit', function (req, res){
    if(req.query.movie_id == null) {
        res.send('An movie_id is required');
    }
    else {
        movie_dal.edit(req.query.movie_id, function(err, result){
            res.render('movie/movieUpdate', {'movie': result[0][0]});
        })
    }
});

module.exports = router;