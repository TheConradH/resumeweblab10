var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM director;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(director_ig, callback){
    var query = 'SELECT * FROM director WHERE director_id = ?';
    var querydData = [director_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.insert = function(params, callback){
    var query = 'INSERT INTO director (first_name) VALUES(?)';

    var queryData = [params.first_name, params.last_name, params.age];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.update = function(params, callback){
    var query = 'UPDATE director SET first_name = ?, last_name = ?, age = ? WHERE director_id = ?';
    var queryData = [params.first_name, params.last_name, params.age, params.director_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};


exports.delete = function(director_id, callback){
    var query = 'DELETE FROM director WHERE director_id = ?';
    var queryData = [director_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(director_id, callback){
    var query = 'CALL director_get_info(?)';
    var queryData = [director_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};