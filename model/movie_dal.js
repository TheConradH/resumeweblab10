var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM movie;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(movie_ig, callback){
    var query = 'SELECT * FROM movie WHERE movie_id = ?';
    var querydData = [movie_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.insert = function(params, callback){
    var query = 'INSERT INTO movie (name) VALUES(?)';

    var queryData = [params.name, params.year];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.update = function(params, callback){
    var query = 'UPDATE movie SET name = ?, year = ? WHERE movie_id = ?';
    var queryData = [params.name, params.year, params.movie_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};


exports.delete = function(movie_id, callback){
    var query = 'DELETE FROM movie WHERE movie_id = ?';
    var queryData = [movie_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(movie_id, callback){
    var query = 'CALL movie_get_info(?)';
    var queryData = [movie_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};