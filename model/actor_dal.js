var mysql   = require('mysql');
var db  = require('./db_connection.js');

/* DATABASE CONFIGURATION */
var connection = mysql.createConnection(db.config);

exports.getAll = function(callback) {
    var query = 'SELECT * FROM actor;';

    connection.query(query, function(err, result) {
        callback(err, result);
    });
};

exports.getById = function(actor_ig, callback){
    var query = 'SELECT * FROM actor WHERE actor_id = ?';
    var querydData = [actor_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.insert = function(params, callback){
    var query = 'INSERT INTO actor (first_name) VALUES(?)';

    var queryData = [params.first_name, params.last_name, params.age];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.update = function(params, callback){
    var query = 'UPDATE actor SET first_name = ?, last_name = ?, age = ? WHERE actor_id = ?';
    var queryData = [params.first_name, params.last_name, params.age, params.actor_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};


exports.delete = function(actor_id, callback){
    var query = 'DELETE FROM actor WHERE actor_id = ?';
    var queryData = [actor_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};

exports.edit = function(actor_id, callback){
    var query = 'CALL actor_get_info(?)';
    var queryData = [actor_id];

    connection.query(query, queryData, function(err, result){
        callback(err, result);
    });
};